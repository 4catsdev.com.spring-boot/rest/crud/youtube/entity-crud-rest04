package com.fourcatsdev.entitycrudrest.servico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fourcatsdev.entitycrudrest.modelo.Estudante;
import com.fourcatsdev.entitycrudrest.repositorio.EstudanteRepositorio;

@Service
public class EstudanteServico {
	
	@Autowired
	private EstudanteRepositorio estudanteRepositorio;
	
	public Estudante gravar(Estudante estudante) {
		return estudanteRepositorio.save(estudante);
	}

}

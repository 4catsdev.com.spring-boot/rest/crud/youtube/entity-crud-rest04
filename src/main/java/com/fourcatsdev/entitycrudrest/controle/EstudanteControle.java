package com.fourcatsdev.entitycrudrest.controle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fourcatsdev.entitycrudrest.dto.EstudanteCreateDTO;
import com.fourcatsdev.entitycrudrest.dto.EstudanteResponseDTO;
import com.fourcatsdev.entitycrudrest.dto.mapper.EstudanteMapper;
import com.fourcatsdev.entitycrudrest.modelo.Estudante;
import com.fourcatsdev.entitycrudrest.servico.EstudanteServico;

@RestController
@RequestMapping("/estudantes")
public class EstudanteControle {
	
	@Autowired
	private EstudanteServico estudanteServico;
	
	@Autowired
	private EstudanteMapper estudanteMapper;
	
	@PostMapping
    public ResponseEntity<EstudanteResponseDTO> salvar(@RequestBody EstudanteCreateDTO estudanteCreateDTO) {
        Estudante estudante = estudanteMapper.toEntity(estudanteCreateDTO);
        Estudante estudanteGravado = estudanteServico.gravar(estudante);
        EstudanteResponseDTO estudanteResponseDTO = estudanteMapper.toDTO(estudanteGravado);        
        return ResponseEntity.status(HttpStatus.CREATED).body(estudanteResponseDTO);
    }

}
